package guru.springframework.webapp.controllers;

import guru.springframework.model.ShippingAddress;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShippingAddressController {

  @RequestMapping("/shipping-address")
  public ShippingAddress getShippingAddress(){
    return new ShippingAddress();
  }
}
